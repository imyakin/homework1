/* Task 1 */
console.log('Task 1');
const firstName = "Алексей";
let age = 23,
    familyStatus = true,
    children = false;


console.log(`First name: ${firstName}`);
console.log("Age: " + age);
console.log("Married: " + familyStatus);
console.log("Have children: " + children);


/* Task 2 */
console.log('Task 2');
let heigh = 23,
    width = 10,
    square = (heigh + width) /2;
console.log(square);

/* Task 3 */
console.log('Task 3');
let cylinder = {
    heigh: 10,
    width: 4
}

console.log('Cylinder square = ' + (cylinder.heigh * cylinder.width));

/* Task 4 */
console.log('Task 4');
/*
Infinity - "1" number
"42" + 42 sting
2 + "1 1" string
99 + 101  number
"1" - "1" string
"Result: " + 10/2 string
3 + " bananas " + 2 + " apples " string
*/
console.log(typeof (Infinity - "1"));
console.log(typeof ("42" + 42));
console.log(typeof (2 + "1 1"));
console.log(typeof (99 + 101));
console.log(typeof ("1" - "1"));
console.log(typeof ("Result: " + 10/2));
console.log(typeof (3 + " bananas " + 2 + " apples "));